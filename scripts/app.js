document.addEventListener('DOMContentLoaded', () => {
    let list = [];

    const quantityInput = document.querySelector('.shopping-quantity__input');
    const incrementButton = document.querySelector('.shopping-quantity__button--increment');
    const decrementButton = document.querySelector('.shopping-quantity__button--decrement');
    const itemName = document.querySelector('.shopping-item__input')
    const form = document.querySelector('.shopping-form');

    const items = document.querySelector('.shopping-items');


    incrementButton.addEventListener('click', () => {
        const currentValue = Number(quantityInput.value);
        const newValue = currentValue + 1;
        quantityInput.value = newValue;
    });

    decrementButton.addEventListener('click', () => {
        const currentValue = Number(quantityInput.value);
        const newValue = currentValue - 1;

        if (currentValue > 1) {
            quantityInput.value = newValue;
        }
    })

    form.addEventListener('submit', (e) => {
        e.preventDefault();

        const item = {
            name: itemName.value,
            quantity: quantityInput.value
        }

        if (itemName.value !== '') {
            list.push(item)
        }

        renderList();
        formatList();
    })

    const renderList = () => {
        let itemsStructure = '';

        list.forEach(item => {
            itemsStructure = `
                <li class='shopping-item'>
                    <span>${item.name}</span>
                    <span>${item.quantity}</span>
                </li>
            `
        });

        items.innerHTML += itemsStructure;
    }

    const formatList = () => {
        quantityInput.value = '1';
        itemName.value = '';
    }

})